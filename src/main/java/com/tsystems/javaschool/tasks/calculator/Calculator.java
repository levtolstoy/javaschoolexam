package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result;

        try {
            result = format(eval(statement));
        } catch (Exception e){
            result = null;
        }

        return result;
    }

    private static double eval(String statement) throws Exception {
        char [] input = statement.toCharArray();
        double result = 0;
        String temp = "";

        ArrayList<String> actions = new ArrayList<String>();
        ArrayList<Double> opers = new ArrayList<Double>();

        for (int i = 0; i < input.length; i++) {
            if (input[i] == '('){
                String subInput = "";
                for (int j = i+1; j < input.length; j++) {
                    if (input[j] != ')'){
                        subInput = subInput + String.valueOf(input[j]);
                    } else {
                        break;
                    }
                }
                temp = Double.toString(eval(subInput));
                i = i+subInput.length()+2;
            }

            if ((i == 0) && (input[i] == '-')){
                opers.add(-1.0);
                actions.add("*");
            } else if (Character.isDigit(input[i]) || input[i] == '.'){
                temp = temp + String.valueOf(input[i]);
            } else if ((input[i] == '+')
                    || (input[i] == '-')
                    || (input[i] == '*')
                    || (input[i] == '/')){
                opers.add(Double.parseDouble(temp));
                actions.add(String.valueOf(input[i]));
                temp = "";
            } else {
                throw new Exception();
            }
        }

        opers.add(Double.parseDouble(temp));

        for (int i = 0; i < actions.size(); i++) {
            if (actions.get(i).equals("*")){
                opers.set(i, opers.get(i) * opers.get(i+1));
                opers.remove(i+1);
                actions.remove(i);
                i=-1;
            } else if (actions.get(i).equals("/")){
                opers.set(i, opers.get(i) / opers.get(i+1));
                opers.remove(i+1);
                actions.remove(i);
                i=-1;
            }
        }

        for (int i = 0; i < actions.size(); i++) {
            if (actions.get(i).equals("+")){
                opers.set(i, opers.get(i) + opers.get(i+1));
                opers.remove(i+1);
                actions.remove(i);
                i=-1;
            } else if (actions.get(i).equals("-")){
                opers.set(i, opers.get(i) - opers.get(i+1));
                opers.remove(i+1);
                actions.remove(i);
                i=-1;
            }
        }

        result = Double.parseDouble(String.valueOf(opers.get(0)));

        return result;
    }

    private static String format(double d) {
        String result = "";
        String[] splitter = String.valueOf(d).split("\\.");

        if ((d % 1) == 0){
            result = String.valueOf((int) d);
        } else if (splitter[1].length() <= 4){
            result = String.valueOf(d);
        } else {
            result = String.format("%.4f", d);
        }

        return result;
    }

}
