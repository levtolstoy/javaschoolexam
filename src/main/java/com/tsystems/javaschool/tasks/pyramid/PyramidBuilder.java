package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.size() >= 255){
            throw new CannotBuildPyramidException();
        }

        List<Integer> input = new ArrayList<>(inputNumbers);
        List<int[]> tempList = new ArrayList<int[]>();
        int[][] pyramid;


        try {
            sort(input); //сортируем

            for (int i = 0; i < input.size(); i++) { //делим на список массивов
                int[] tmp = new int[i+1];
                    for (int j = 0; j < tmp.length; j++) {
                        tmp[j] = input.get(j);
                    }

                tempList.add(i, tmp);

                    for (int j = 0; j < i+1; j++) {
                        input.remove(0);
                    }

            }

            pyramid = new int[tempList.size()][tempList.size()*2-1];

            for (int i = pyramid.length; i > 0 ; i--) {
                for (int j = 0; j < i; j++) {
                    pyramid[i-1][j] = tempList.get(i-1)[j];
                }
            }

        } catch (Exception e){
            throw new CannotBuildPyramidException();
        }

        for (int i = 0; i < pyramid.length; i++) {
            zeroSort(i,pyramid[pyramid.length-i-1]);
        }

        return pyramid;
    }

    private void zeroSort(int iter, int[] mas) {
        int t = 0;
        int l = mas.length - 1;

        for (int i = l; i > 0; i--) { // считаем нули
            if (mas[i] == 0){
                t++;
            }
        }

        t = t - iter; //делаем отступ

        for (int i = l; i >= 0; i--) { //сдвигаем ненулевые числа
            if (mas[i] != 0){
                mas[i+t]= mas[i];
                if (t!=0){
                    mas[i] = 0;
                }
                t--;
            }
        }
    }

    private void sort(List<Integer> inputNumbers) { //bubble sort
        for (int i = inputNumbers.size() -1; i >=0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (inputNumbers.get(j) > inputNumbers.get(j+1)){
                    int t = inputNumbers.get(j);
                    inputNumbers.set(j, inputNumbers.get(j+1));
                    inputNumbers.set(j+1, t);
                }
            }
        }
    }


}
